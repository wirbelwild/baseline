(function() {

    "use strict";

    function init() {

        // Methods/polyfills.

        // addEventsListener
        var addEventsListener = function(o, t, e) {
            var n,
                i = t.split(" ");
            for (n in i) {
                o.addEventListener(i[n], e);
            }
        }

        // Vars.
        var $body = document.querySelector("body");

        // Disable animations/transitions until everything"s loaded.
        $body.classList.add("is-loading");

        window.addEventListener("load", function() {
            $body.classList.remove("is-loading");
        });

        // Nav.
        var $nav = document.querySelector("#nav"),
            $navToggle = document.querySelector("a[href='#nav']"),
            $navClose;

        if ($nav !== null) {

            // Event: Prevent clicks/taps inside the nav from bubbling.
            addEventsListener($nav, "click touchend", function(event) {
                event.stopPropagation();
            });

            // Event: Hide nav on body click/tap.
            addEventsListener($body, "click touchend", function(event) {
                $nav.classList.remove("visible");
            });

            // Toggle.

            // Event: Toggle nav on click.
            $navToggle.addEventListener("click", function(event) {
                event.preventDefault();
                event.stopPropagation();
                $nav.classList.toggle("visible");
            });

            // Close.

            // Create element.
            $navClose = document.createElement("a");
            $navClose.href = "#";
            $navClose.className = "close";
            $navClose.tabIndex = 0;
            $nav.appendChild($navClose);

            // Event: Hide on ESC.
            window.addEventListener("keydown", function(event) {
                if (event.keyCode == 27) {
                    $nav.classList.remove("visible");
                }
            });

            // Event: Hide nav on click.
            $navClose.addEventListener("click", function(event) {
                event.preventDefault();
                event.stopPropagation();
                $nav.classList.remove("visible");
            });
        }
    };

    document.addEventListener("DOMContentLoaded", function() {
        init();
    }, false);
}());
