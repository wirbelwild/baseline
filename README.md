# Baseline

__Baseline__ is a simple boilerplate for building new projects with the help of [Rows](https://bitbucket.org/wirbelwild/rows). 

Features include:

- Clear and concise HTML5/CSS3 code.
- Fully responsive.
- Extensively organized Sass sources.
- Off-canvas navigation (built with CSS3 and just a tiny bit of vanilla JS).
- Numerous pre-styled elements.
- Everything offered by __Rows__, including:
    - A powerful, fully responsive CSS grid system.
    - Handy utility classes (like `container`).

## Installation

This package is available for [composer](https://packagist.org/packages/bitandblack/baseline) and also for [node](https://www.npmjs.com/package/bitandblack-baseline).

### Composer 

Install __rows__ by running `$ composer require bitandblack/baseline` and `$ composer update`.

### NodeJS

Install __rows__ by running `$ npm install bitandblack-baseline`.

## How to use

### Breakpoint Structure

Baseline is set up to use the following breakpoints:

Name*        | Media Query
-------------|-----------------------
`x-large`    | `(max-width: 1680px)`
`large`      | `(max-width: 1280px)`
`medium`     | `(max-width: 980px)`
`small`      | `(max-width: 736px)`
`x-small`    | `(max-width: 480px)`

Read the documentation of __Rows__ to see, how this breakpoints can be changed.

## Credits

This version of __Baseline__ is based of the work of [Ajlkn](https://github.com/ajlkn/baseline).
